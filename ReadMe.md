## Exercise on C++/CUDA and Basic Machine Learning Algorithm (k-means)

### Usage

```sh
# clone the project
git clone https://gitlab.com/longc/cuda-kmeans.git --recursive
cd cuda-kmeans

# install requirements
pip install -r requirements.txt

# make the project

# run benchmark
python bench_kmeans.py

# output (the cuda version is not implemented and the result is just a Placeholder)
n_digits: 10,      n_samples 1797,      n_features 64
__________________________________________________________________________________
init        time    inertia    homo    compl    v-meas    ARI    AMI    silhouette
sklearn      0.239s    69432    0.602    0.650    0.625    0.465    0.598    0.146
cpu          0.367s    69735    0.669    0.715    0.692    0.557    0.666    0.139
cuda         0.080s    10000    0.035    0.035    0.035    0.014    0.026    -0.057
__________________________________________________________________________________
```

The sklearn version is implemented in cython and the cpu version is implemented in C++. You can find the source code in `clib/src`.

### Problems

1. As you can see, current C++ version is slower than sklearn version, so please check the C++ source code in `clib/src/KMeans_cpu.cpp` and optimize it using any method such as OpenBlas, Eigen, and cpu parallelization.

2. Please implement the cuda version in `clib/src/KMeans_cuda.cpp` and `clib/src/KMeans_kernel.cu`.

3. Sometimes, we may want to speed up python code with C++/CUDA extensions.
However as you can see in the last line of output, it costs about 0.08s for an empty C++ function.
This may caused by the overhead of converting python list ot C++ vector. Please check the reason.
One possible solution is to direct input a NxM feature matrix (N samples with M features for each) instead of
using a list of M-d feature vector.

4. Please write a document about your optimization and implementation. Profiling the C++/CUDA code is welcome. 
