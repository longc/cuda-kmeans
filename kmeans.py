import numpy as np
from sklearn.cluster import KMeans as KMeans_sklearn

from clib import KMeansCPU, KMeansCUDA


class KMeans(object):
    def __init__(self, kernel, n_clusters, n_init=10, max_iter=300):
        self.kernel = kernel
        self.n_clusters = n_clusters
        self.n_init = n_init
        self.max_iter = max_iter
        self.tol = 1e-5

        if self.kernel == 'sklearn':
            self._estimator = KMeans_sklearn(self.n_clusters, 'k-means++',
                                             n_init=self.n_init, max_iter=self.max_iter, tol=self.tol)
        elif self.kernel == 'cpu':
            self._estimator = KMeansCPU
        elif self.kernel == 'cuda':
            self._estimator = KMeansCUDA
        else:
            raise ValueError('Unknown KMeans kernel: {}'.format(self.kernel))

    def fit(self, data):
        if self.kernel == 'sklearn':
            self._estimator.fit(data)
            inertia, labels = self._estimator.inertia_, self._estimator.labels_
        elif self.kernel in {'cpu', 'cuda'}:
            best_labels = None
            best_inertia = np.inf
            for i in range(self.n_init):
                inertia, labels = self._estimator(data, self.n_clusters, self.max_iter, self.tol)
                if inertia < best_inertia:
                    best_inertia = inertia
                    best_labels = labels
            inertia, labels = best_inertia, best_labels

        return inertia, labels
