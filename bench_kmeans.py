"""
===========================================================
A demo of K-Means clustering on the handwritten digits data
===========================================================

Modified from https://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html

In this example we compare the various initialization strategies for
K-means in terms of runtime and quality of the results.

As the ground truth is known here, we also apply different cluster
quality metrics to judge the goodness of fit of the cluster labels to the
ground truth.

Cluster quality metrics evaluated (see :ref:`clustering_evaluation` for
definitions and discussions of the metrics):

=========== ========================================================
Shorthand    full name
=========== ========================================================
homo         homogeneity score
compl        completeness score
v-meas       V measure
ARI          adjusted Rand index
AMI          adjusted mutual information
silhouette   silhouette coefficient
=========== ========================================================

"""
from time import time
import numpy as np

from sklearn import metrics
from sklearn.datasets import load_digits
from sklearn.preprocessing import scale

from kmeans import KMeans

np.random.seed(42)

digits = load_digits()
data = scale(digits.data)

n_samples, n_features = data.shape
n_digits = len(np.unique(digits.target))
labels = digits.target

sample_size = 300

print("n_digits: %d, \t n_samples %d, \t n_features %d"
      % (n_digits, n_samples, n_features))


print(82 * '_')
print('init\t\ttime\tinertia\thomo\tcompl\tv-meas\tARI\tAMI\tsilhouette')


def bench_k_means(estimator, name, data):
    t0 = time()
    inertia, predicts = estimator.fit(data)
    print('%-9s\t%.3fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
          % (name, (time() - t0), inertia,
             metrics.homogeneity_score(labels, predicts),
             metrics.completeness_score(labels, predicts),
             metrics.v_measure_score(labels, predicts),
             metrics.adjusted_rand_score(labels, predicts),
             metrics.adjusted_mutual_info_score(labels,  predicts),
             metrics.silhouette_score(data, predicts, metric='euclidean', sample_size=sample_size)))


bench_k_means(KMeans(kernel='sklearn', n_clusters=n_digits, n_init=10), name="sklearn", data=data)
bench_k_means(KMeans(kernel='cpu', n_clusters=n_digits, n_init=10), name="cpu", data=data.tolist())
bench_k_means(KMeans(kernel='cuda', n_clusters=n_digits, n_init=10), name="cuda", data=data.tolist())

print(82 * '_')

