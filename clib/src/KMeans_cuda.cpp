#include "KMeans.h"


std::pair<double, std::vector<int> > KMeansCUDA(std::vector<DoubleArray> &data,
        const int &n_clusters, const int &max_iter, const double &tol) {
    const long n_features = data[0].request().shape[0];
    std::vector<const double *> data_ptrs(data.size());
    for (size_t i = 0; i < data.size(); ++i) {
        data_ptrs[i] = static_cast<const double *>(data[i].request().ptr);
    }

    // TODO: implement kmeans in cuda
    std::vector<int> cluster_indices(data.size(), 0);
    for (int i = 0; i < data.size(); ++i) {
        cluster_indices[i] = i % n_clusters;
    }
    double inertia = 10000;
    return std::make_pair(inertia, cluster_indices);
}