#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

namespace py = pybind11;

typedef py::array_t<double, py::array::c_style | py::array::forcecast> DoubleArray;    // dtype=np.float64


std::pair<double, std::vector<int> > KMeansCPU(std::vector<DoubleArray> &data,
        const int &n_clusters, const int &max_iter, const double &tol);

std::pair<double, std::vector<int> > KMeansCUDA(std::vector<DoubleArray> &data,
        const int &n_clusters, const int &max_iter, const double &tol);