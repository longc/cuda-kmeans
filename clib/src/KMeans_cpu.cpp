#include "KMeans.h"
#include <random>
#include <vector>
#include <cstring>
#include <limits>


double squared_l2_distance(const double *pa, const double *pb, const long n) {
    double sum = 0;
    for (size_t i = 0; i < n; ++i) {
        sum += pow((pa[i] - pb[i]), 2);
    }
    return sum;
}


void vector_add(double *dest, const double *src, const long n) {
    for (size_t i = 0; i < n; ++i) {
        dest[i] += src[i];
    }
}


void vector_mul(double *dest, const double mul, const long n) {
    for (size_t i = 0; i < n; ++i) {
        dest[i] *= mul;
    }
}


void copy_means(std::vector<double *> dests, std::vector<double *> srcs, const long n) {
    for (size_t i = 0; i < srcs.size(); ++i) {
        std::memcpy(dests[i], srcs[i], n * sizeof(double));
    }
}


std::pair<int, double> find_nearest_centroid(const double *pdata, std::vector<double *> means, const long n_features) {
    double best_distance = std::numeric_limits<double>::max();
    int best_cluster = 0;
    for (int i = 0; i < means.size(); ++i) {
        const double distance = squared_l2_distance(pdata, means[i], n_features);
        if (distance < best_distance) {
            best_cluster = i;
            best_distance = distance;
        }
    }
    return std::make_pair(best_cluster, best_distance);
}


std::pair<double, std::vector<int> > KMeansCPU(std::vector<DoubleArray> &data, const int &n_clusters,
        const int &max_iter, const double &tol) {
    const long n_features = data[0].request().shape[0];
    std::vector<const double *> data_ptrs(data.size());
    for (size_t i = 0; i < data.size(); ++i) {
        data_ptrs[i] = static_cast<const double *>(data[i].request().ptr);
    }

    // randomly select centroids of clusters from original data
    static std::random_device seed;
    static std::mt19937 random_number_generator(seed());
    std::uniform_int_distribution<size_t> indices(0, data.size() - 1);

    std::vector<double *> means(n_clusters);
    std::vector<double *> prev_means(n_clusters);
    for (size_t i = 0; i < n_clusters; ++i) {
        means[i] = new double[n_features];
        std::memcpy(means[i], data_ptrs[indices(random_number_generator)], n_features * sizeof(double));
        prev_means[i] = new double[n_features];
    }

    // iteration
    std::vector<int> cluster_indices(data.size());
    for (size_t iteration = 0; iteration < max_iter; ++iteration) {
        // find assignments
        for (size_t i = 0; i < data.size(); ++i) {
            cluster_indices[i] = find_nearest_centroid(data_ptrs[i], means, n_features).first;
        }

        // update centroids
        copy_means(prev_means, means, n_features);
        for (double *mean : means) {
            std::memset(mean, 0, n_features * sizeof(double));
        }
        std::vector<int> counts(means.size(), 0);
        for (size_t i = 0; i < data.size(); ++i) {
            const int cluster_index = cluster_indices[i];
            vector_add(means[cluster_index], data_ptrs[i], n_features);
            counts[cluster_index]++;
        }
        for (size_t i = 0; i < means.size(); ++i) {
            vector_mul(means[i], 1. / counts[i], n_features);
        }

        // check convergence
        double center_shift_total = 0;
        for (size_t i = 0; i < means.size(); ++i) {
            double center_shift = squared_l2_distance(prev_means[i], means[i], n_features);
            center_shift_total += sqrt(center_shift);
        }
        if (center_shift_total * center_shift_total < tol)
            break;
    }

    // calculate labels and distances
    double inertia = 0;
    for (size_t i = 0; i < data.size(); ++i) {
        auto rtn = find_nearest_centroid(data_ptrs[i], means, n_features);
        cluster_indices[i] = rtn.first;
        inertia += rtn.second;
    }

    // free centroids
    for (double *mean : means) {
        free(mean);
    }
    for (double *mean : prev_means) {
        free(mean);
    }

    return std::make_pair(inertia, cluster_indices);
}