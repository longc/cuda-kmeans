#include "KMeans.h"


PYBIND11_MODULE(_C, m) {
    m.doc() = "KMeans Lib";

    m.def("KMeansCPU", &KMeansCPU);
    m.def("KMeansCUDA", &KMeansCUDA);
}